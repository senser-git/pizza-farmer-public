if getreg().pizza_farmer_loaded then return end;
getreg().pizza_farmer_loaded = true;

if game.PlaceId ~= 192800 then messagebox("Please execute this in \"work at a pizza place\"", "pizza-farmer", 0x10) return end;

local Players = game:GetService("Players");
local LocalPlayer = Players.LocalPlayer;
local Workspace = game:GetService("Workspace");
local HttpService = game:GetService("HttpService");
local ReplicatedStorage = game:GetService("ReplicatedStorage");
local ScriptContext = game:GetService("ScriptContext");
local RunService = game:GetService("RunService");

local gsub = string.gsub;
local lower = string.lower;
local format = string.format;

local pcall = pcall;
local coroutine_wrap = coroutine.wrap;
local wait = wait;

local coSpawn = function(f, ...) coroutine_wrap(f)(...) end;

local utils = {};
local handlers = {};


local function join_server(code)
    return syn.request({
        Url = "http://127.0.0.1:6463/rpc?v=1";
        Method = "POST";
        Body = format(
            '{"args":{"code":"%s"},"nonce":"%s","cmd":"INVITE_BROWSER"}', 
            code, 
            lower(HttpService:GenerateGUID())
        );
        Headers = {
            ["Content-Type"] = "application/json",
            ["Origin"] = "https://discord.com"
        }
    });
end;

local state = {}; do 
    --// @note - this might have potentinal mem leaks so
    state.__index = state;

    function state.new() 
        return setmetatable({
            flags = {};
            callbacks = {};
            currentFlag = "";
        }, state);
    end;

    function state:disableCurrentFlag() 
        if self.currentFlag == "" then return end;
        local callback = self.callbacks[self.currentFlag];

        self.flags[self.currentFlag] = false;
        coSpawn(callback, false);
    end;

    function state:enable(name) 
        if self.currentFlag == name then return end; --// to prevent bugs

        local callback = self.callbacks[name];
        self:disableCurrentFlag();
        
        self.flags[name] = true;
        coSpawn(callback, true);

        self.currentFlag = name;
    end;

    function state:reset() 
        self:disableCurrentFlag();
        self.currentFlag = "";
    end;

    function state:define(options) 
        options = options or { callback = function() end, flag = "" };

        self.flags[options.flag] = false;
        self.callbacks[options.flag] = options.callback;
    end;
end;

local RBX = { -- rbx game context
    Positions = { 
        PizzaPlace = CFrame.new(20, 5, -14);
        Registers = CFrame.new(48, 4, 85);
        BoxerTable = CFrame.new(66, 5, 22);
    };

    PlaceId = 192800;
    library = loadstring(game:HttpGet("https://raw.githubusercontent.com/Stroketon/uwuware-ui/main/main.lua"))();
    Customers = workspace.Customers;
    autoFarmState = state.new();
    handledCustomers = {};
    handledFood = {};
    belts = Workspace.BoxingRoomBelts;
    PayCheck = LocalPlayer.PlayerGui.MainGui.Prompts.Paycheck;
    Network = require(ReplicatedStorage.LibraryFolder.Network);
};

RBX.Utils = { }; do 
    function RBX.Utils.getCustomer()
        local customers = RBX.Customers:GetChildren();

        for i = 1, #customers do
            local v = customers[i];
            if (not v:FindFirstChild("Head") or not v.Head:FindFirstChild("Dialog") or not v.Head.Dialog:FindFirstChild("Correct") or not v:FindFirstChild("Humanoid") or not LocalPlayer.Character or not LocalPlayer.Character:FindFirstChild("HumanoidRootPart") or not v:FindFirstChild("HumanoidRootPart") or v.Humanoid.WalkSpeed > 0) then continue end;
            local distance = (v.HumanoidRootPart.Position - LocalPlayer.Character.HumanoidRootPart.Position).Magnitude;

            if (distance <= 18) then --> I think this should fix the NPCs breaking?
                return v;
            end;
        end;
    end;

    function RBX.Utils.getFood()
        local food = Workspace.BoxingRoom:GetChildren();
        return (#food > 0 and food[1]);
    end;

    function RBX.Utils.getPizzaBox()
        local boxes = Workspace.AllBox:GetChildren();

        for i = 1, #boxes do
            local v = boxes[i];
            local pizzaType;
    
            if (v:FindFirstChild("HasPizzaInside") or v:FindFirstChild("Pizza")) then
                if (v.Name == "BoxOpen" and v.Pizza.Value or v.Name == "BoxClosed" and v.HasPizzaInside.Value) then
                    pizzaType = "Full"; --> Boxed and ready to ship.
                elseif (v.Name == "BoxOpen" and not v.Pizza.Value and not v.Anchored) then
                    pizzaType = "Open"; --> Opened box.
                elseif (not v.HasPizzaInside.Value and v.Name == "BoxClosed" and not v.Anchored) then
                    pizzaType = "Closed"; --> Just a closed box.
                end;
            end;

            return v, pizzaType;
        end;
    end;

    function RBX.Utils.setAutoFarmOption(option) 
        RBX.autoFarmState:enable(
            option == "Suppiler" and "supply" or option == "Cashier" and "cashier" or option == "Pizza Boxer" and "boxer"
        );
    end;
end;

do -- utils
    function utils.checkTeam(teamName) 
        return function(flag) 
            if LocalPlayer.Team == game.Teams.Manager or LocalPlayer.Team == game.Teams[teamName] or not flag then return end;

            ReplicatedStorage.PlayerChannel:FireServer("ChangeJob", teamName);
        end;
    end;

    function utils.lpisOnTeam(team) 
        return LocalPlayer.Team == team or 
            LocalPlayer.Team == game.Teams.Manager;
    end;
end;

RBX.autoFarmState:define({
    flag = "cashier",
    callback = function(flag) 
        if flag then LocalPlayer.Character.HumanoidRootPart.CFrame = RBX.Positions.Registers end;
        LocalPlayer.Character.Humanoid.WalkSpeed = flag and 0 or 16;
        
        utils.checkTeam("Cashier")(flag);
    end;
});

RBX.autoFarmState:define({
    flag = "supply",
    callback = utils.checkTeam("Supplier");
});

RBX.autoFarmState:define({
    flag = "boxer",
    callback = utils.checkTeam("Pizza Boxer");
});

do -- Events
    Workspace.DescendantAdded:Connect(function(instance)
        if instance.Name ~= "SupplyBox" or not RBX.autoFarmState.flags.supply then return end;
        RBX.Network:FireServer("UpdateProperty", instance, "CFrame", RBX.Positions.PizzaPlace);
    end);
end;

do -- Loops
    coroutine_wrap(function() -- supply auto farm loop
        while true do 
            local SupplyButtons = Workspace.SupplyButtons:GetChildren();

            for i = 1, #SupplyButtons do 
                local Button = SupplyButtons[i];
                if RBX.autoFarmState.flags.supply then 
                    LocalPlayer.Character.HumanoidRootPart.CFrame = Button.Unpressed.CFrame;
                    LocalPlayer.Character.Humanoid.Jump = true;

                    wait(0.40);
                end;
            end;

            wait();
        end;
    end)();

    coroutine_wrap(function()
        while wait() do
            local customer = RBX.Utils.getCustomer();

            if (customer and RBX.autoFarmState.flags.cashier and not table.find(RBX.handledCustomers, customer)) then
                local correct, order = customer.Head.Dialog.Correct.ResponseDialog, "MountainDew";
            
                if (correct:find("cheese")) then
                    order = "CheesePizza";
                elseif (correct:find("pepp")) then
                    order = "PepperoniPizza";
                elseif (correct:find("sausage")) then
                    order = "SausagePizza";
                end;
                
                RBX.Network:FireServer("OrderComplete", customer, order, Workspace.Register1);
                table.insert(RBX.handledCustomers, customer);
            end;
        end;
    end)();

    coroutine_wrap(function() --> This auto farm really pissed me off during development.
        while true do
            local food = RBX.Utils.getFood();
    
            if (food and RBX.autoFarmState.flags.boxer and not table.find(RBX.handledFood, food)) then
                if (food.Name == "Dew") then
                    RBX.Network:FireServer("UpdateProperty", food, "CFrame", RBX.belts.DewBelt.CFrame);
                    table.insert(RBX.handledFood, food);
                else
                    -- Update propety is slow so I gotta spam waits everywhere :sob: (either that or I was on a laggy server :shrug:)
                    local pizzaBox, pizzaType = RBX.Utils.getPizzaBox();

                    if (pizzaBox) then
                        if (pizzaType == "Full") then --> Move completed pizza to conveyor.
                            if (pizzaBox.Name == "BoxOpen") then
                                RBX.Network:FireServer("CloseBox", pizzaBox);
                            else
                                RBX.Network:FireServer("UpdateProperty", pizzaBox, "CFrame", RBX.belts.BoxBelt.CFrame);
                                wait(2); --> Stop loop TPing, would use tables but I rely on this loop running multiple times.
                            end;
                        elseif (pizzaType == "Open") then --> Open box, no pizza.
                            RBX.Network:FireServer("UpdateProperty", pizzaBox, "Anchored", true);
                            RBX.Network:FireServer("UpdateProperty", food, "Anchored", true);
                            wait();
                            RBX.Network:FireServer("UpdateProperty", food, "CFrame", pizzaBox.CFrame - Vector3.new(0, 2, 0)); --> I stole this Vector3 from game code xddd.
                            wait();
                            RBX.Network:FireServer("AssignPizzaToBox", pizzaBox, food);
                        else --> Closed box.
                            RBX.Network:FireServer("UpdateProperty", pizzaBox, "CFrame", RBX.Positions.BoxerTable);
                            wait();
                            RBX.Network:FireServer("OpenBox", pizzaBox);
                        end;
                    end;
                end;
            end;
            wait();
        end;
    end)();
end;

do -- anti afk & anti error log
    local idleConnections = getconnections(LocalPlayer.Idled);
    --local errorConnections = getconnections(ScriptContext.Error);
    
    idleConnections[1]:Disable();
    --errorConnections[1]:Disable(); --> Error logger found in "ReplicatedStorage.LibraryFolder.ErrorLoggerLocal"
end;

local autoFarmSection = RBX.library:CreateWindow("Auto Farm");
local creditsSection = RBX.library:CreateWindow("Credits");

autoFarmSection:AddToggle({
    text = "Enabled";
    flag = "autofarm";
    callback = function(flag)
        if flag then return RBX.Utils.setAutoFarmOption(RBX.library.flags.job_option) end;
        RBX.autoFarmState:reset();
    end;
});

do -- auto paycheck
    local signal = getconnections(workspace.Main.GivePaycheck.OnClientEvent)[1].Function;
    local CashOut = getupvalue(signal, 17);
    local fakeInstance = setmetatable({}, {
        __index = function() return RunService.Heartbeat end;
    });

    autoFarmSection:AddToggle({
        text = "Auto PayCheck";
        callback = function(flag) 
            setupvalue(signal, 17, flag and fakeInstance or CashOut);
        end;
    });
end;

autoFarmSection:AddList({
    text = "Jobs";
    values = { "Cashier", "Suppiler", "Pizza Boxer" };
    flag = "job_option";
    callback = function(option) 
        if not RBX.library.flags.autofarm then return end;
        RBX.Utils.setAutoFarmOption(option);
    end;
});


local libSettings = RBX.library:CreateWindow("Settings");
libSettings:AddButton({
    text = "Join Discord Server";
    callback = function() 
        join_server("F8fua9hxra");
    end;
});

creditsSection:AddLabel({ text = "senser#1337 - dev" });
creditsSection:AddLabel({ text = "Spencer#0003 - co dev" });
creditsSection:AddLabel({ text = "MikeyA#5511 - assistance" });

RBX.library:Init();
